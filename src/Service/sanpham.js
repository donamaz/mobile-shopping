import axios from "axios";

const apiUrl = "https://localhost:7220/api/Sanpham";

export const getAllSanphams = async () => {
  try {
    const response = await axios.get(`${apiUrl}/GetAllSP`);
    return response.data;
  } catch (error) {
    console.error("Failed to fetch all products", error);
    throw error;
  }
};

export const getSanphamById = async (id) => {
  try {
    const response = await axios.get(`${apiUrl}/GetAllSP/${id}`);
    return response.data;
  } catch (error) {
    console.error(`Failed to fetch product with id ${id}`, error);
    throw error;
  }
};

export const addSanpham = async (sanpham) => {
  try {
    const response = await axios.post(`${apiUrl}/AddSanpham`, sanpham);
    return response.data;
  } catch (error) {
    console.error("Failed to add product", error);
    throw error;
  }
};

export const updateSanpham = async (id, sanpham) => {
  try {
    await axios.put(`${apiUrl}/UpdateSanpham/${id}`, sanpham);
  } catch (error) {
    console.error(`Failed to update product with id ${id}`, error);
    throw error;
  }
};

export const deleteSanpham = async (id) => {
  try {
    await axios.delete(`${apiUrl}/DeletSanpham/${id}`);
  } catch (error) {
    console.error(`Failed to delete product with id ${id}`, error);
    throw error;
  }
};

export const searchSanpham = async (keyword) => {
  try {
    const response = await axios.get(`${apiUrl}/search`, {
      params: { keyword },
    });
    return response.data;
  } catch (error) {
    console.error(`Failed to search products with keyword ${keyword}`, error);
    throw error;
  }
};

export const getSanphamByLoaiSp = async (loaiSpId) => {
  try {
    const response = await axios.get(`${apiUrl}/loaisp/${loaiSpId}`);
    return response.data;
  } catch (error) {
    console.error(`Failed to fetch products by category id ${loaiSpId}`, error);
    throw error;
  }
};

export const getTop10KhuyenMai = async () => {
  try {
    const response = await axios.get(`${apiUrl}/khuyenmai`);
    return response.data;
  } catch (error) {
    console.error("Failed to fetch top 10 discount products", error);
    throw error;
  }
};

export const getTop10MoiNhat = async () => {
  try {
    const response = await axios.get(`${apiUrl}/moi`);
    return response.data;
  } catch (error) {
    console.error("Failed to fetch top 10 new products", error);
    throw error;
  }
};
export const searchSanpham2 = async (keyword) => {
  try {
    const response = await axios.get(`${apiUrl}/search2?keyword=${keyword}`);
    return response.data;
  } catch (error) {
    console.error(`Failed to search products with keyword ${keyword}`, error);
    throw error;
  }
};
export const filterSanphams = async (
  minPrice,
  maxPrice,
  minRating,
  maxRating,
) => {
  try {
    const response = await axios.get(
      "https://localhost:7220/api/Sanpham/filter",
      {
        params: {
          minPrice,
          maxPrice,
          minRating,
          maxRating,
        },
      },
    );
    return response.data;
  } catch (error) {
    throw new Error("Failed to filter products");
  }
};
